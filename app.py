# GNU General Public License v2.0 or later (GPL-2.0-or-later)
#
# Copyright (c) 2022 Jukka Peranto
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, render_template, request, jsonify
from kirkkovuosilaskuri import Kirkkovuosi
from datetime import datetime
import json
import os
import tomllib

app = Flask("Kirkkovuosikalenteri")


with open("kirkkovuoden_pyhapaivat.toml", "rb") as f:
    pyhapaivat = tomllib.load(f)


@app.route("/")
def home():
    return render_template("kalenteri.html")


@app.route("/testi")
def testi():
    return render_template("testi.html")


@app.route("/kalenteri", methods=["GET"])
def kalenteri():
    alkupvm = datetime.fromisoformat(request.args.get("start"))
    loppupvm = datetime.fromisoformat(request.args.get("end"))
    if alkupvm.year == loppupvm.year:
        tapahtumalista = Kirkkovuosi(alkupvm.year).pyhapaivat
    else:
        tapahtumalista = Kirkkovuosi(alkupvm.year, loppupvm.year).pyhapaivat
    return jsonify(tapahtumalista)


@app.route("/pyha/<pyhapaiva>/<vuosikerta>", methods=["GET"])
def pyhapaiva(pyhapaiva, vuosikerta):
    tekstit = pyhapaivat[pyhapaiva][vuosikerta]
    nimi = pyhapaivat[pyhapaiva]["nimi"]
    psalmit = pyhapaivat[pyhapaiva]["ps"]
    return render_template("pyhapaiva.html", paiva=pyhapaiva, vk=vuosikerta, tekstit=tekstit, nimi=nimi, psalmit=psalmit)

@app.route("/tiedot", methods=["GET"])
def tiedot():
    pyha = request.args.get("pyha")
    vk = str(request.args.get("vk"))
    tyokansio = os.path.dirname(__file__)
    tiedosto = open(os.path.join(tyokansio, "lektionaari.json"))
    lektionaari = json.load(tiedosto)
    # print(lektionaari[pyha][vk])
    return render_template("tiedot.html", tiedot=lektionaari[pyha][vk])
#

if __name__ == "__main__":
    app.run(debug=True)
