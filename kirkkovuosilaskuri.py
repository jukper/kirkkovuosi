# GNU General Public License v2.0 or later (GPL-2.0-or-later)
#
# Copyright (c) 2022 Jukka Peranto
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License

from datetime import date, timedelta
import json
import os.path


class Kirkkovuosi:
    def __init__(self, alkuvuosi: int, loppuvuosi=None):
        self.__tyokansio = os.path.dirname(__file__)
        self.__pyhatiedosto = open(os.path.join(self.__tyokansio, "pyhapaivat.json"))
        self.__tunnukset = json.load(self.__pyhatiedosto)
        self.alkuvuosi = alkuvuosi
        self.vuosikerrat = [2, 3, 1]
        self.vuosikerta = self.vuosikerrat[self.alkuvuosi % 3]
        self.pyhapaivat = []

        if loppuvuosi:
            self.loppuvuosi = loppuvuosi

            for vuosi in range(self.alkuvuosi, self.loppuvuosi + 1):
                self.laske_paivat(vuosi, self.vuosikerta)
                if self.vuosikerta < 3:
                    self.vuosikerta += 1
                else:
                    self.vuosikerta = 1

        else:
            self.laske_paivat(self.alkuvuosi, self.vuosikerta)

        self.pyhapaivat.sort(key=lambda lista: (lista["start"], lista["paino"]))

    def laske_paivat(self, vuosi: int, vuosikerta: int):
        self.__tmpvuosi = []
        self.kiinteat_paivat(vuosi, vuosikerta)
        self.laske_paasiainen(vuosi, vuosikerta)
        self.adventti(vuosi, vuosikerta)
        self.joulu(vuosi, vuosikerta)
        self.laske_paastonaika(vuosi, vuosikerta)
        self.laske_loppiaisaika(vuosi, vuosikerta)
        self.laske_helluntaiaika(vuosi, vuosikerta)
        for paiva in self.__tmpvuosi:
            self.pyhapaivat.append(paiva)

        # IDEA: Olisiko fiksumpaa tallentaa kertaalleen lasketut pyhäpäivät tiedostoon?
        # with open(
        #     os.path.join(self.__tyokansio, f"vuodet/{vuosi}.json", "w")
        # ) as outfile:
        #     json.dump(self.__tmpvuosi, outfile)

    def lisaa_paiva(
        self,
        nimi,
        vuosikerta: int,
        pvm: date,
        paino: int = 0,
        vari: str = "vihreä",
        kynttilat: int = 2,
    ):
        if vuosikerta == 0:
            vuosikerta = 3
        varit = {
            "vihreä": "green",
            "valkoinen": "white",
            "punainen": "red",
            "violetti": "purple",
            "musta": "black",
        }
        pyhapaiva = {}
        pyhapaiva["id"] = f"{pvm.year}-{self.__tunnukset[nimi]}"
        pyhapaiva["pyhaid"] = self.__tunnukset[nimi]
        pyhapaiva["title"] = nimi
        pyhapaiva["vk"] = vuosikerta
        pyhapaiva["vari"] = vari
        pyhapaiva["kynttilat"] = kynttilat
        pyhapaiva["start"] = pvm.isoformat()
        # pyhäpäivälle annetaan paino, jotta samalle päivälle
        # sijoittuvat pyhät saadaan oikeaan järjestykseen
        pyhapaiva["paino"] = paino
        pyhapaiva["url"] = f"/pyha/{self.__tunnukset[nimi]}/{vuosikerta}"
        pyhapaiva["backgroundColor"] = varit[vari]
        if vari == "valkoinen":
            pyhapaiva["textColor"] = "black"
        self.__tmpvuosi.append(pyhapaiva)

    def kiinteat_paivat(self, vuosi: int, vuosikerta: int):
        self.__kiinteatiedosto = open(
            os.path.join(self.__tyokansio, "kiinteat_paivat.json")
        )

        kiinteat_paivat = json.load(self.__kiinteatiedosto)

        paivat_dict = kiinteat_paivat.items()
        for paiva in paivat_dict:
            nimi = str(paiva[0])
            pvm = paiva[1]["pvm"].split("-")
            kk = int(pvm[0])
            pv = int(pvm[1])
            vk_mod = int(pvm[2])
            pvari = paiva[1]["vari"]
            pkynttilat = paiva[1]["kynttilat"]

            self.lisaa_paiva(
                nimi,
                vuosikerta - vk_mod,
                date(vuosi, kk, pv),
                vari=pvari,
                kynttilat=pkynttilat,
            )

    def adventti(self, vuosi, vuosikerta):
        # 1. adventti on aina aikaisintaan 27.11.
        adventtiraja = date(vuosi, 11, 27).isoweekday()

        if adventtiraja == 7:
            self.adventti1pvm = date(vuosi, 11, 27)
        else:
            self.adventti1pvm = date(vuosi, 11, 27) + timedelta(7 - adventtiraja)
        self.lisaa_paiva(
            "1. adventtisunnuntai",
            vuosikerta,
            self.adventti1pvm,
            vari="valkoinen",
            kynttilat=6,
        )

        # 2., 3., ja 4. adventti on helppo laskea

        for n in range(3):
            self.lisaa_paiva(
                f"{n+2}. adventtisunnuntai",
                vuosikerta,
                self.adventti1pvm + timedelta(7 + 7 * n),
                vari="violetti",
                kynttilat=2,
            )

    def joulu(self, vuosi, vuosikerta):
        # Selvitetään joulunjälkeiset sunnuntait

        # 1. mahdollinen joulunjälkeinen sunnuntai on tapaninpäivä
        self.sunjoulusta1pvm = date(vuosi, 12, 26) + timedelta(
            7 - date(vuosi, 12, 26).isoweekday()
        )
        self.sunjoulusta2pvm = (
            date(vuosi - 1, 12, 26)
            + timedelta(7 - date(vuosi - 1, 12, 26).isoweekday())
            + timedelta(7)
        )

        if self.sunjoulusta1pvm not in [
            date(vuosi, 12, 26),
            date(vuosi, 12, 27),
            date(vuosi, 12, 28),
        ]:
            self.lisaa_paiva(
                "1. sunnuntai joulusta",
                vuosikerta,
                self.sunjoulusta1pvm,
                vari="valkoinen",
                kynttilat=4,
            )

        if self.sunjoulusta2pvm < date(vuosi, 1, 6):
            self.lisaa_paiva(
                "2. sunnuntai joulusta",
                vuosikerta,
                self.sunjoulusta2pvm,
                vari="valkoinen",
                kynttilat=4,
            )

    def laske_paasiainen(self, vuosi, vuosikerta):
        # Pääsiäinen lasketaan nk. Butcherin algoritmilla
        # https://en.wikipedia.org/wiki/Date_of_Easter#Anonymous_Gregorian_algorithm=
        a = vuosi % 19
        b = vuosi // 100
        c = vuosi % 100
        d = (19 * a + b - b // 4 - ((b - (b + 8) // 25 + 1) // 3) + 15) % 30
        e = (32 + 2 * (b % 4) + 2 * (c // 4) - d - (c % 4)) % 7
        f = d + e - 7 * ((a + 11 * d + 22 * e) // 451) + 114
        kuukausi = f // 31
        paiva = f % 31 + 1
        self.paasiainen_pvm = date(vuosi, kuukausi, paiva)
        self.lisaa_paiva(
            "Pääsiäispäivä", vuosikerta - 1, self.paasiainen_pvm, vari="valkoinen"
        )

        self.lisaa_paiva(
            "Pääsiäisyö",
            vuosikerta - 1,
            self.paasiainen_pvm - timedelta(1),
            1,
            vari="valkoinen",
            kynttilat=6,
        )

        paasiaisviikko = [
            "2. pääsiäispäivä",
            "Pääsiäisen jälkeinen tiistai",
            "Pääsiäisen jälkeinen keskiviikko",
            "Pääsiäisen jälkeinen torstai",
            "Pääsiäisen jälkeinen perjantai",
            "Pääsiäisen jälkeinen lauantai",
        ]

        for n in range(len(paasiaisviikko)):
            self.lisaa_paiva(
                paasiaisviikko[n],
                vuosikerta - 1,
                self.paasiainen_pvm + timedelta(1 + n),
                vari="valkoinen",
                kynttilat=4,
            )

        paasiaisen_jalkeen = [
            "1. sunnuntai pääsiäisestä (Quasimodogeniti)",
            "2. sunnuntai pääsiäisestä (Misericordia Domini)",
            "3. sunnuntai pääsiäisestä (Jubilate)",
            "4. sunnuntai pääsiäisestä (Cantate)",
            "5. sunnuntai pääsiäisestä (Rukoussunnuntai, Rogate)",
            "6. sunnuntai pääsiäisestä (Exaudi)",
        ]
        for n in range(len(paasiaisen_jalkeen)):
            self.lisaa_paiva(
                paasiaisen_jalkeen[n],
                vuosikerta - 1,
                self.paasiainen_pvm + timedelta((n + 1) * 7),
                vari="valkoinen",
                kynttilat=4,
            )

        self.lisaa_paiva(
            "Helatorstai",
            vuosikerta - 1,
            self.paasiainen_pvm + timedelta(39),
            vari="valkoinen",
            kynttilat=6,
        )

    def laske_paastonaika(self, vuosi, vuosikerta):
        # Ensin lasketaan paastoa edeltävien päivien päivämäärät
        self.septuagesima_pvm = self.paasiainen_pvm - timedelta(63)
        self.sexagesima_pvm = self.septuagesima_pvm + timedelta(7)
        self.esto_mihi_pvm = self.septuagesima_pvm + timedelta(14)

        # Tämän jälkeen täytyy laskea kynttilänpäivä, vaikka se ei varsinaisesti ole paastopäivä

        self.kynttilanpaiva_pvm = date(vuosi, 2, 2) + timedelta(
            7 - date(vuosi, 2, 2).isoweekday()
        )
        if self.kynttilanpaiva_pvm == self.esto_mihi_pvm:
            self.kynttilanpaiva_pvm = self.esto_mihi_pvm - timedelta(7)

        # Sijoitetaan kynttilänpäivä ja paastoa edeltävät sunnuntait paikoilleen

        self.lisaa_paiva(
            "Laskiaissunnuntai (Esto mihi)",
            vuosikerta - 1,
            self.esto_mihi_pvm,
            vari="vihreä",
            kynttilat=2,
        )

        if self.septuagesima_pvm != self.kynttilanpaiva_pvm:
            self.lisaa_paiva(
                "3. sunnuntai ennen paastonaikaa (Septuagesima)",
                vuosikerta - 1,
                self.septuagesima_pvm,
                vari="vihreä",
                kynttilat=2,
            )

        if self.sexagesima_pvm != self.kynttilanpaiva_pvm:
            self.lisaa_paiva(
                "2. sunnuntai ennen paastonaikaa (Sexagesima)",
                vuosikerta - 1,
                self.sexagesima_pvm,
                vari="vihreä",
                kynttilat=2,
            )

        self.lisaa_paiva(
            "Kynttilänpäivä",
            vuosikerta - 1,
            self.kynttilanpaiva_pvm,
            vari="valkoinen",
            kynttilat=6,
        )
        self.lisaa_paiva(
            "Tuhkakeskiviikko",
            vuosikerta - 1,
            self.esto_mihi_pvm + timedelta(3),
            vari="violetti",
            kynttilat=2,
        )

        # Paastonajan sunnuntaissa tulee huomioida Marian ilmestyspäivä.
        # Siksi se lasketaan esin.

        self.marianpaiva_pvm = date(vuosi, 3, 22) + timedelta(
            7 - date(vuosi, 3, 22).isoweekday()
        )
        if self.marianpaiva_pvm in [
            self.paasiainen_pvm,
            self.paasiainen_pvm - timedelta(7),
        ]:
            self.marianpaiva_pvm = self.paasiainen_pvm - timedelta(14)

        self.lisaa_paiva(
            "Marian ilmestyspäivä",
            vuosikerta - 1,
            self.marianpaiva_pvm,
            vari="valkoinen",
            kynttilat=6,
        )

        paastonajansun = [
            "1. paastonajan sunnuntai (Invocavit)",
            "2. paastonajan sunnuntai (Reminiscere)",
            "3. paastonajan sunnuntai (Oculi)",
            "4. paastonajan sunnuntai (Laetare)",
            "5. paastonajan sunnuntai (Judica)",
            "Palmusunnuntai",
        ]

        for n in range(len(paastonajansun)):
            pvm = self.esto_mihi_pvm + timedelta(7 * (n + 1))
            if pvm != self.marianpaiva_pvm:
                self.lisaa_paiva(
                    paastonajansun[n], vuosikerta - 1, pvm, vari="violetti", kynttilat=2
                )

        hiljainen_viikko = [
            "Hiljaisen viikon maanantai",
            "Hiljaisen viikon tiistai",
            "Hiljaisen viikon keskiviikko",
            "Kiirastorstai",
            "Pitkäperjantai",
        ]
        for n in range(len(hiljainen_viikko)):
            nvari = "violetti"
            kynttilalkm = 2
            if hiljainen_viikko[n] == "Pitkäperjantai":
                nvari = "musta"
                kynttilalkm = 0
            pvm = self.paasiainen_pvm - timedelta(7)
            self.lisaa_paiva(
                hiljainen_viikko[n],
                vuosikerta - 1,
                pvm + timedelta(n + 1),
                vari=nvari,
                kynttilat=kynttilalkm,
            )

        self.lisaa_paiva(
            "Jeesuksen kuolinhetki",
            vuosikerta - 1,
            pvm + timedelta(5),
            paino=1,
            vari="musta",
            kynttilat=0,
        )

        self.lisaa_paiva(
            "Pitkäperjantain ilta",
            vuosikerta - 1,
            pvm + timedelta(5),
            paino=2,
            vari="musta",
            kynttilat=0,
        )

        self.lisaa_paiva(
            "Hiljainen lauantai",
            vuosikerta - 1,
            pvm + timedelta(6),
            vari="musta",
            kynttilat=0,
        )

    def laske_loppiaisaika(self, vuosi, vuosikerta):
        self.loppiainen_pvm = date(vuosi, 1, 6)
        loppiaisen_jalkeen = self.loppiainen_pvm + timedelta(1)
        tmppaiva = loppiaisen_jalkeen + timedelta(7 - loppiaisen_jalkeen.isoweekday())

        tmpnro = 1

        tmpraja = self.septuagesima_pvm

        while tmppaiva < tmpraja:
            if tmppaiva != self.kynttilanpaiva_pvm:
                pvari = "vihreä"
                kynttilalkm = 2
                if tmpnro == 1:
                    pvari = "valkoinen"
                    kynttilalkm = 4
                self.lisaa_paiva(
                    f"{tmpnro}. sunnuntai loppiaisesta",
                    vuosikerta - 1,
                    tmppaiva,
                    vari=pvari,
                    kynttilat=kynttilalkm,
                )
            tmpnro += 1
            tmppaiva += timedelta(7)

    def laske_helluntaiaika(self, vuosi, vuosikerta):
        # Helluntaiaika loppuu juuri ennen uutta adventtia

        # Lopusta taaksepäin voimme laskea sekä tuomiosunnuntain että valvomisen sunnuntain

        self.tuomiosun_pvm = self.adventti1pvm - timedelta(7)

        self.lisaa_paiva(
            "Tuomiosunnuntai (Kristuksen kuninkuuden sunnuntai)",
            vuosikerta - 1,
            self.tuomiosun_pvm,
            vari="vihreä",
            kynttilat=2,
        )

        self.valvomisensun_pvm = self.tuomiosun_pvm - timedelta(7)

        self.lisaa_paiva(
            "Valvomisen sunnuntai",
            vuosikerta - 1,
            self.valvomisensun_pvm,
            vari="vihreä",
            kynttilat=2,
        )

        # Helluntaisjaksoon kuuluu myös Mikkelinpäivä, joka 29.9. seuraava sunnuntai

        self.mikkelinpaiva_pvm = date(vuosi, 9, 29) + timedelta(
            7 - date(vuosi, 9, 29).isoweekday()
        )

        self.lisaa_paiva(
            "Mikkelinpäivä (Enkelien sunnuntai)",
            vuosikerta - 1,
            self.mikkelinpaiva_pvm,
            vari="valkoinen",
            kynttilat=4,
        )

        # Pyhäinpäivä on launtaina 31.10-6.11
        pvm = date(vuosi, 10, 31)
        pvmdelta = pvm.isoweekday()
        if pvmdelta == 7:
            pvmdelta = 14
        self.pyhainpaivapvm = pvm + timedelta(6 - pvmdelta)

        self.lisaa_paiva(
            "Pyhäinpäivä",
            vuosikerta - 1,
            self.pyhainpaivapvm,
            vari="punainen",
            kynttilat=4,
        )

        # Juhannus on 20.–26.6. lauantaina

        pvm = date(vuosi, 6, 20)
        pvmdelta = pvm.isoweekday()
        if pvmdelta == 7:
            pvmdelta = 0
        self.juhannuspvm = pvm + timedelta(6 - pvmdelta)

        self.lisaa_paiva(
            "Juhannuspäivä (Johannes Kastajan päivä)",
            vuosikerta - 1,
            self.juhannuspvm,
            vari="valkoinen",
            kynttilat=4,
        )

        # Helluntai on 50. päivä pääsiäisestä

        self.lisaa_paiva(
            "Helluntaiaatto",
            vuosikerta - 1,
            self.paasiainen_pvm + timedelta(48),
            vari="punainen",
            kynttilat=4,
        )

        self.helluntaipvm = self.paasiainen_pvm + timedelta(49)

        self.lisaa_paiva(
            "Helluntaipäivä",
            vuosikerta - 1,
            self.helluntaipvm,
            vari="punainen",
            kynttilat=6,
        )

        self.lisaa_paiva(
            "Pyhän Kolminaisuuden päivä",
            vuosikerta - 1,
            self.helluntaipvm + timedelta(7),
            vari="valkoinen",
            kynttilat=6,
        )

        # Helluntaipäivän jälkeen tulevat sunnuntait lasketaan helluntaipäivästä aina valvomisen sunnuntaihin asti
        paivat = (self.valvomisensun_pvm - self.helluntaipvm).days

        for n in range(14, paivat, 7):
            jarjestysnro = n // 7

            if jarjestysnro == 6:
                self.lisaa_paiva(
                    "Apostolien päivä",
                    vuosikerta - 1,
                    self.helluntaipvm + timedelta(n),
                    vari="punainen",
                    kynttilat=4,
                )

            elif jarjestysnro == 8:
                self.lisaa_paiva(
                    "Kirkastussunnuntai (Kristuksen kirkastumisen päivä)",
                    vuosikerta - 1,
                    self.helluntaipvm + timedelta(n),
                    vari="valkoinen",
                    kynttilat=4,
                )

            elif jarjestysnro == 22:
                self.lisaa_paiva(
                    "Uskonpuhdistuksen muistopäivä",
                    vuosikerta - 1,
                    self.helluntaipvm + timedelta(n),
                    vari="vihreä",
                    kynttilat=2,
                )

            elif self.helluntaipvm + timedelta(n) == self.mikkelinpaiva_pvm:
                continue

            else:
                self.lisaa_paiva(
                    f"{jarjestysnro}. sunnuntai helluntaista",
                    vuosikerta - 1,
                    self.helluntaipvm + timedelta(n),
                    vari="vihreä",
                    kynttilat=2,
                )


def testi():
    # Testi kahdella eri vuodella
    kirkkovuosi = Kirkkovuosi(2022)
    print(f"Vuosikerta on {kirkkovuosi.vuosikerta}")
    print(kirkkovuosi.pyhapaivat)


#
#
# testi()
