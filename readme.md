# Kirkkovuosilaskuri (POC)

Kirkkovuosilaskuri kokeiluluonteinen ohjelma, jonka tarkoituksena on luoda mahdollisuus automaattiselle kirkkovuosikalenterin toteuttamiselle.

Laskurin lisäksi projektiin kuuluu python flaskilla toteutettu api sekä fullcalendar.io-kalenteri.

Kirkkovuoden pyhäpäivät ja niiden laskukaavat pohjautuvat Pentti Lempiäisen _Pyhät ajat_-kirjan tietoihin.

## Ohjelman toiminta

Ohjelma sisältää Kirkkovuosi-luokan, jolle syötetään joko yksittäinen kirkkovuosi, jonka pyhäpäivät lasketaan, tai alku- ja loppuvuodet halutulle aikavälille, jonka pyhäpäivät lasketaan.

Kustakin pyhäpäivästä luodaan oma tietue, johon sisältyvät _pyhäpäivän nimi_, _pyhäpäivän vuosikerta_, _pyhäpäivän tunnus_ sekä _paino_.
Ohjelma noutaa _pyhäpäivän nimen_ avulla erillisestä tiedostosta nimeä vastaavan tunnuksen.
Tästä syystä jokainen pyhäpäivä, joka halutaan ohjelmalla laskea, tulee löytyä pyhäpäivät sisältävästä tiedostosta.
Oletuksena pyhäpäivätiedosto on _pyhapaivat.json_.

Ohjelma laskee vuosikerran kunakin kalenterivuonna alkavan kirkkovuoden mukaan.

Esimerkiksi: Ohjelma laskee vuoden 2022 vuosikerraksi 2.
Näin ollen vuoden 2022 pyhäpäivien vuosikerta on 1. adventtisunnuntaista lähtien 2.
Ennen 1. adventtisunnuntaita olevien vuoden 2022 pyhäpäivien vuosikerta on _vuosikerta - 1_ eli 1.

## Huomautuksia

- Kirkkovuosilaskurin koodia ei ole lainkaan optimoitui. Monissa kohdin voisi pyhäpäivien laskemiset tehdä fiksummin.

- Kalenterin koodi vaatii myös optimointia. Tällä hetkellä jokainen päivän / viikon / kuukauden vaihto kalenterissa saa aikaan kokonaisen vuoden pyhäpäivien uudelleenlaskemisen.
